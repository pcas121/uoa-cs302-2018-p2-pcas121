#This is an abstract class, for hashing passwords, usernames, and encrypting/decrypting messages & files

import bcrypt
import Crypto
from Crypto.PublicKey import RSA
import abc

gen_salt = "$2b$12$Wk7pi6Ir9F/FpKqhrh/Q6OzI0tVFUr0QCFoaLjuj/78.mK19epfei" #Only use this salt for bcrypt hashing

class EncryptAndHash(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def Hash_String(self, string_input, salt):

        assert isinstance(salt, str)
        assert isinstance(string_input, str)

        new_salt = bcrypt.hashpw(salt, gen_salt)

        hashed_string = bcrypt.hashpw(string_input, new_salt)

        return hashed_string

