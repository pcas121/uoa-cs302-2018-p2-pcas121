import Crypto
from Crypto.PublicKey import RSA
import ast

a = (2**(2**2))+1
b = (2**(2**3))+1

e = long(7)
n = long(a*b)
d = long(3511)

in_tuple = (n, e, d)

key = RSA.construct(in_tuple)

print "Key is: ", key.publickey()

init_message = "123, ABC, 123, baby you and me"

print "Initial message is: ", init_message

encrypted = key.publickey().encrypt(init_message, 1024)

print "Encrypted message is: ", encrypted
#print "Encrypted type is: ", type(encrypted)

#decrypted_message = key.decrypt(ast.literal_eval(str(encrypted)))
decrypted_message = key.decrypt(str(encrypted))

print "Decrypted message is: ", decrypted_message #Might have problem with encryption