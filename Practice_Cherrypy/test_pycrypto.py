import Crypto

from Crypto.PublicKey import RSA
from Crypto import Random
import ast

rand_num = Random.new().read
key = RSA.generate(1024, rand_num)
print "key is: ", key.publickey()
print ""

init_message = "No one can decrypt this. MUAHAHA!!!"

publickey = key.publickey()
encrypted = publickey.encrypt(init_message, 32)
print encrypted

decrypted = key.decrypt(ast.literal_eval(str(encrypted)))
print ""
print decrypted