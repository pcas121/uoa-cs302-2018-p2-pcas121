

def is_prime(input_num):
    if not(type(input_num) is int or type(input_num) is long):
        return

    if input_num == 2:
        return True

    for x in range(3, long(input_num**0.5)+1, 2):
        if input_num%x == 0:
            return False

    return True

def is_relatively_prime(in_a, in_b):

    if not (type(int_a) is int or type(int_a) is long):
        print "the first input number is not an integer"
        return
    elif int_a <= 0:
        print "the first input needs to be greater than zero"
        return

    if not (type(int_b) is int or type(int_b) is long):
        print "the second input number is not an integer"
        return
    elif int_b <= 0:
        print "the second number needs to be greater than zero"
        return

    first_factors = []
    for x in range(2, (int_a / 2) + 1):
        if int_a % x is 0:
            first_factors.append(x)
    first_factors.append(int_a)

    second_factors = []
    for x in range(2, (int_b / 2) + 1):
        if int_b % x is 0:
            second_factors.append(x)
    second_factors.append(int_b)

    common_factor_list = []
    common_factor_list = list(set(first_factors).intersection(second_factors))

    if len(common_factor_list) is 0:
        return True
    return False

def encrypt(e, n, plaintext):

    if n < 255:
        print "Error: n is too small"
        return
    if e <= 3:
        print "e is very small. Please choose a greater value for e"
        return

    ciphertext = []

    for x in range(0, len(plaintext)):

        plain_num = ord(plaintext[x])
        cipher_num = (plain_num**e)%n
        ciphertext.append(cipher_num)

    return ciphertext

def decrypt(d, n, ciphertext):

    if n < 255:
        print "Error: n is too small"
        return

    decrypted_text = ''

    for x in range(0, len(ciphertext)):

        cipher_num = ciphertext[x]
        decrypted_num = (cipher_num**d)%n
        decrypted_text += chr(decrypted_num)

    return decrypted_text

orig_str = 'Here is an input string'

print "orig_str is: ", orig_str
print ""

p = (2**(2**2)) + 1
q = (2**(2**3)) + 1
n = p*q
e = 7
d = 3511

encrypted_str = encrypt(e, n, orig_str)

print "Encrypted string is a list. Here's the list: ", encrypted_str
print ""

decrypted_str = decrypt(d, n, encrypted_str)

print "Here's the decrypted message: ", decrypted_str
print ""