def encryption(input_string, e, N):

	if not(type(input_string) is str):
		print "input_string is not a string"
		return
	if not(type(e) is int or type(e) is long):
		print "e's type is wrong"
		return
	if not(type(N) is int or type(N) is long):
		print "N's type is wrong"
		return

	#N = 187
	#e = 7
	output_string = ''
	
	for x in range(0, len(input_string)):
		temp_char = ord(input_string[x])
		encrypted_num = (temp_char**e)%N
		output_string += chr(encrypted_num)
		
	return output_string
	
def decryption(input_string, d, N):

	if not(type(input_string) is str):
		print "input_string is not a string"
		return
	if not(type(d) is int or type(d) is long):
		print "d's type is wrong"
		return
	if not(type(N) is int or type(N) is long):
		print "N's type is wrong"
		return

	#N = 187
	#d = 23
	output_string = ''
	
	for x in range(0, len(input_string)):
		temp_char = ord(input_string[x])
		decrypted_num = (temp_char**d)%N
		output_string += chr(decrypted_num)
		
	return output_string
	
def is_relatively_prime(int_a, int_b):

	#try:
	#	first_num = int(int_a)
	#	second_num = int(int_b)
	#except ValueError:
	#	print "One of your inputs is not an integer"
	if not(type(int_a) is int or type(int_a) is long):
		print "the first input number is not an integer"
		return
	elif int_a <= 0:
		print "the first input needs to be greater than zero"
		return
	
	if not(type(int_b) is int or type(int_b) is long):
		print "the second input number is not an integer"
		return
	elif int_b <= 0:
		print "the second number needs to be greater than zero"
		return
	
	first_factors = []
	for x in range(2, (int_a/2)+1):
		if int_a%x is 0:
			first_factors.append(x)
	first_factors.append(int_a)
			
	second_factors = []
	for x in range(2, (int_b/2)+1):
		if int_b%x is 0:
			second_factors.append(x)
	second_factors.append(int_b)
			
	common_factor_list = []
	common_factor_list = list(set(first_factors).intersection(second_factors))
	
	if len(common_factor_list) is 0:
		return True
	else:
		return False
		
def make_decode_key(e, N, p, q):

	if not(type(e) is int or type(e) is long):
		print "e's type is wrong"
		return
	elif not(type(N) is int or type(N) is long):
		print "N's type is wrong"
		return
	elif not(type(p) is int or type(p) is long):
		print "p's type is wrong"
		return
	elif not(type(q) is int or type(q) is long):
		print "q's type iw wrong"
		return
			
	d = 0		
	while not((e*d)%((p-1)*(q-1)) is 1):
		d = d + 1
		
	return d
	
def is_prime(input_int):

	if not(type(input_int) is int or type(input_int) is long):
		print "input type is wrong"
		return
		
	if input_int is 2:
		return True
		
	if input_int < 2:
		return False;
		
	for x in range(2, long(input_int**0.5)+1):
		if input_int%x is 0:
			return False
			
	return True

stringie = "Bet you cannot decrypt this"
print "Initial message is: " + stringie
stringie = encryption(stringie, 7, 187)
print "Encrypted message is :" + stringie
stringie = decryption(stringie, 23, 187)
print "Decrypted message is: " + stringie

#if __name__ is "__main__":
#		main()
		
#def main():

#	print "RUNNING..."

#	stringie = "Bet you cant decrypt this"
#	print "Initial message is " + stringie
#	stringie = encrypt(stringie)
#	print "encrypted string is " + stringie
#	stringie = decrypt(stringie)
#	print "decrypted string is " + stringie