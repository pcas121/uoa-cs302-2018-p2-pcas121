#import Crypto
from Crypto.Hash import SHA256
from Crypto import Random
#from Crypto import Random

#def hashing_256(input_string, number_input):

h1 = SHA256.new()
message = "Here's a test string!"
h1.update(message)
print "Initial message is: " + message
print ""
print "Hashed message is: " + h1.hexdigest()
print ""

salty_message = "HAVE SOME SALT!!! MUAHAHA!!!"
salty_message += message
h2 = SHA256.new(salty_message)
print "The salted and hashed message is: " + h2.hexdigest()
print ""

if len(h1.hexdigest()) == len(h2.hexdigest()):
    print "Hashes have the same length, even when one is salted"
    print ""
else:
    print "Original hash and salted hash do not have the same length"
    print

rand_num = Random.new().read(16)
print type(rand_num)
print "len of rand_num is: " + str(len(rand_num)) + " bytes"
for x in range(0, len(rand_num)):
    print ord(rand_num[x])

h3 = SHA256.new(rand_num + message)
print ""
print "Message salted with random string is: " + h3.hexdigest()
