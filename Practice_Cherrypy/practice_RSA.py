import Crypto
from Crypto.PublicKey import RSA
from Crypto import Random
import ast

random_generator = Random.new().read
key = RSA.generate(2048, random_generator)

publickey = key.publickey()

encrypted = publickey.encrypt('encrypt this message', 32)

print 'Encrypted message: ', encrypted
f = open('encryption.txt', 'w')
f.write(str(encrypted))
f.close()

f = open('encryption.txt', 'r')
message = f.read()

decrypted = key.decrypt(ast.literal_eval(str(encrypted)))

print 'Decrypted: ', decrypted

f = open('encryption.txt', 'w')
f.write(str(message))
f.write(str(decrypted))
f.close()

print "Key object type is: ", type(key)

print "Key has private component: ", key.has_private()
print ""
print ""