import hashlib
import urllib
import urllib2
import socket
import cherrypy

listening_ip = socket.gethostbyname(socket.gethostname())
listening_port = 10001

# test=hashlib.sha256()
# test.update(b"adultskeletonpcas121")
# print "salted and hashed password is: ", test.hexdigest()

class MainApp(object):

	_cp_config = {'tools.encode.on': True,
			'tools.encode.encoding': 'utf8',
			'tools.sessions.on': 'True',
			}
	
	@cherrypy.expose
	def default(self, *args, **kwargs):
		Page = "Unknown error. Returning error 404"
		cherrypy.response.status = 404
		return Page

	def hash_password(self, plain_pass, username):
		assert isinstance(plain_pass, str)
		assert isinstance(username, str)

		plain_pass += username

		pass_hash = hashlib.sha256()
		pass_hash.update(plain_pass)
		return pass_hash.hexdigest()

	@cherrypy.expose
	def index(self):
		Page = ("<html><head></head><body>Welcome to the CS302 Webpage of 2018<br/><br/>Click here to <a href='login_page'> login</a>.<br/><br/></body></html>")
		return Page


	@cherrypy.expose
	def login(self, username, password, location=0, ip="0"):
	
		username = str(username)
		password = str(password)
		
		#print type(location)
		#print "location as an int is: " + str(ord(location))
		location = ord(location)
		location = location-48
	
		#assert isinstance(username, str)
		#assert isinstance(password, str)
		assert isinstance(location, int)

		if (ip is "0"):
			ip = self.get_ip_address()
			#print "IP address is: " + ip

		hash_pass = self.hash_password(password, username)

		params = urllib.urlencode(
		{'username': username, 'password': hash_pass, 'location': str(location), 'ip': str(ip), 'port': "10001"})
		login = urllib2.urlopen('http://cs302.pythonanywhere.com/report?' + params)

			#Note: COMPARE LOCAL IP ADDRESS TO EXTERNAL IP ADDRESS

			#print login.read()

		login_result = login.read()
		
		#if (username is "pcas121" and hash_pass is "648461aceff6258f2a6dc5d6c9d0fddaa6e2decccb8d061aaf8f4a7612f87423"):
		auth_num = self.authoriseLogin(username, hash_pass)
		if (auth_num == 0):
			cherrypy.session['username']=username
			raise cherrypy.HTTPRedirect('/')
		else:
			raise cherrypy.HTTPRedirect('/login_page')
		
		#if login_result[0] == chr(0):
			
			#return login


	def get_ip_address(self):
		print "IP address is: " + socket.gethostbyname(socket.gethostname())
		return socket.gethostbyname(socket.gethostname())
		
	def authoriseLogin(self, username, hashed_pass):
		username.lower()
		if (username is "pcas121" and hashed_pass is "648461aceff6258f2a6dc5d6c9d0fddaa6e2decccb8d061aaf8f4a7612f87423"):
			return 0
		else: 
			return 1


	def check_online_users(self, username, password):
		assert isinstance(username, str)
		assert isinstance(password, str)

		hash_pass = hashlib.sha256()
		password += username
		hash_pass.update(password)
		password = hash_pass.hexdigest()

		params = urllib.urlencode({'username': username, 'password': password})

		gettingList = urllib2.urlopen('http://cs302.pythonanywhere.com/getList?' + params)

		print gettingList.read()


	def getAPIs(self):
		gettingAPIs = urllib2.urlopen('http://cs302.pythonanywhere.com/listAPI')

		print gettingAPIs.read()


	def listUsers(self):
		listing_users = urllib2.urlopen('http://cs302.pythonanywhere.com/listUsers')

		print listing_users.read()

	def logoff(self, username, password, enc=0):

		#assuming that password is already hashed

		assert isinstance(username, str)
		assert isinstance(password, str)
		assert isinstance(enc, int)

		params = urllib.urlencode({'username':username, 'password':password, 'enc':enc})

		logging_off = urllib2.urlopen("http://cs302.pythonanywhere.com/logoff?" + params)

		print logging_off.read()

	@cherrypy.expose
	def login_page(self):
		Page = '<form action="/login" method="post" enctype="multipart/form-data">'
		Page += 'Username: <input type="text" name="username"/><br/>'
		Page += 'Password: <input type="password" name="password"/><br/>'
		Page += 'Location(optional): <input type="number" name="location"/>'
		#Page += 'IP(optional): <input type="text" name="ip"/>'
		Page += '<input type="submit" value="login"/></form>'
	
		return Page

def runMainApp():

	cherrypy.tree.mount(MainApp(), "/")

	server_ip_add = socket.gethostbyname(socket.gethostname())

	cherrypy.config.update({'server.socket_host': listening_ip,
							'server.socket_port': listening_port,
							'engine.autoreload.on': True})

	cherrypy.engine.start()

	cherrypy.engine.block()


runMainApp()


#cherrypy.quickstart(MainApp(), '/')


#login("pcas121", "adultskeleton", 0)
#check_online_users("pcas121", "adultskeleton")
#getAPIs()
#listUsers()

#hashie = hash_password("adultskeleton", "pcas121")

#logoff("pcas121", hashie)
