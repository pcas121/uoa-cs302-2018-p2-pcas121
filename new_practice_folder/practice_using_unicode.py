#!/usr/bin/python
# -*- coding: utf-8 -*-

japanese_test_string = u'このメッセージが見えるの？'
russian_test_string = u'Я немного говорю по русский'
korean_test_string = u'안녕하세요'

encoded_japanese_message = japanese_test_string.encode('utf-8')


print u'japanese_test_string is: ' + encoded_japanese_message.decode('utf-8')
print u'russian_test_string is: ' + russian_test_string
print u'korean_test_string is: ' + korean_test_string
print u"アルミ缶の上にあるみかん"

print ""
english_string = "Here's an ASCII string"
english_unicode_string = unicode(english_string)
print english_unicode_string
print "Type of English Unicode String is: " + str(type(english_unicode_string))