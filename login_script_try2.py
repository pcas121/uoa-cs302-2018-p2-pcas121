#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
==================================================================
					PCAS121 - PYTHON PROJECT 2018
===================================================================

This is my login_server script. Run it to host a website. Currently, the address of your website is your Wireless LAN
adapter Wi-fi's IPv4 address, followed by ':10001' as you will use port '10001'

ASSUMPTION: All usernames only use ASCII characters. The same is assumed for one's personal biography, the name of
the country they are working in, and their password
"""


import cherrypy
import urllib
import urllib2
import hashlib
import socket
import sqlite3
import json


#listen_ip = "0.0.0.0"
listen_ip = socket.gethostbyname(socket.gethostname())
listen_port = 10001


class MainApp(object):

	_cp_config = {'tools.encode.on': True,
				  'tools.encode.encoding': 'utf-8',
				  'tools.sessions.on': 'True',
				  }

	@cherrypy.expose
	def default(self, *args, **kwargs):

		Page = "Here's a 404 Error."
		cherrypy.response.status = 404
		return Page

	@cherrypy.expose
	def index(self):
		Page = "Welcome! This is a test website for COMPSYS302!<br/>"

		#These need to be in the try section, otherwise there is sortof a vulnerability here
		#self.create_user_table()
		#self.create_messages_table()

		try:

			self.create_user_table()
			self.create_messages_table()

			Page += "Hello " + cherrypy.session['username'] + "!<br/>"

			Page += "You have successfully logged-in!!!<br/><br/>"

			Page += "Click here to see who else is <a href='check_current_users'>online</a><br/><br/>"

			#Page += "Click here to add yourself to a <a href='data_entry_user_table'>database</a>. NOTE: THIS LINE IS FOR TESTING ONLY<br/><br/>"
			self.data_entry_user_table()

			Page += "Click here to change which <a href='change_current_country'>country</a> you are currently working in.<br/><br/>"

			Page += "Click here to update your <a href='change_current_bio'>biography</a><br/><br/>"

			Page += "Click here to <a href='display_profile'>see your profile</a><br/><br/>"

			Page += "Click here to <a href='signout'>signout</a>"

		except KeyError:  # There is no username

			Page += "Click here to <a href='login'>login</a>."

		return Page

	#This is for when a name has successfully been added to the 'user' database
	@cherrypy.expose
	def self_added_to_database(self):

		Page = "You have been successfully added to the database, " + str(cherrypy.session.get('username')) + ".<br/><br/>"
		Page += "Click here to return to your <a href='index'>home page</a>"

		return Page


	@cherrypy.expose
	def check_current_users(self, password=None):

		if (cherrypy.session.get('username') == None):
			raise cherrypy.HTTPRedirect('/')

		#user_name = cherrypy.session['username']
		#hashed_password = self.hashing_password(password, user_name)

		Page = 'Click here to return to <a href="index">main screen</a><br/><br/>'

		Page += 'List of currently online users: <br/>'

		online_users = urllib2.urlopen('http://cs302.pythonanywhere.com/getList?' + 'username=' + cherrypy.session['username'] + '&password=' + cherrypy.session['hashed_password'])

		Page += online_users.read()[29:] #Note, after this line of code, you cannot access anything worthwhile from "online_users"

		return Page


	@cherrypy.expose
	def login(self):
		Page = '<form action="/signin" method="post" enctype="multipart/form-data">'
		Page += 'Username: <input type="text" name="username"/><br/>'
		Page += 'Password: <input type="password" name="password"/><br/>'
		Page += 'Location: <input type="number" name="location"/><br/>'

		Page += '<input type="submit" value="Login"/></form>'

		Page += "Click here to return to the <a href='signout'>initial screen</a>"

		return Page

	def hashing_password(self, password, salt):

		password = str(password)
		salt = str(salt)

		hash_password = hashlib.sha256()
		salty_password = password + salt
		hash_password.update(salty_password)
		return hash_password.hexdigest()

	def retrieve_ip_address(self):
		return socket.gethostbyname(socket.gethostname())


	@cherrypy.expose
	def signin(self, username=None, password=None, location=None):

		password = self.hashing_password(password, username)

		location = str(location)

		#Encode stuff to JSON before sending it to "authoriseUserLogin"
		#output_dict = { "username":username,
		#				""}

		error = self.authoriseUserLogin(username, password, location)
		if (error == 0):

			cherrypy.session['username'] = username
			cherrypy.session['hashed_password'] = password
			cherrypy.session['location'] = location
			raise cherrypy.HTTPRedirect('/')
		else:
			raise cherrypy.HTTPRedirect('/login')

	@cherrypy.expose
	def signout(self):
		username = cherrypy.session.get('username')
		if (username == None):
			pass
		else:

			logout_url = urllib2.urlopen("http://cs302.pythonanywhere.com/logoff?username=" + cherrypy.session.get('username') + "&password=" + cherrypy.session.get('hashed_password'))

			print ""
			print logout_url.read()
			print ""

			cherrypy.lib.sessions.expire()
		raise cherrypy.HTTPRedirect('/')

	def authoriseUserLogin(self, username, password, location):

		params = urllib.urlencode({'username': username, 'password': password, 'location': location, 'ip': self.retrieve_ip_address(), 'port': '10001'})
		url_opening = urllib2.urlopen('http://cs302.pythonanywhere.com/report?' + params)

		result_string = url_opening.read()

		print ""
		print result_string
		print ""

		return (ord(result_string[0]) - 48)




	def create_user_table(self):

		conn = sqlite3.connect('user_database.db') #connection
		c = conn.cursor() #cursor

		#usernames should be encrypted, as if username and hashed_password are both known, a person can still login to the person's account via browser

		c.execute("CREATE TABLE IF NOT EXISTS users(enc_username TEXT, country TEXT, personal_bio TEXT, prof_pic BLOB)")
		c.close()
		conn.close()

	#@cherrypy.expose
	def data_entry_user_table(self):
 
		if cherrypy.session.get('username') is None:
			print ""
			print "This user has no cherrypy session"
			print ""
			return
			#raise cherrypy.HTTPRedirect('/')
 
		plain_username = str(cherrypy.session.get('username'))
		#hashed_password = str(cherrypy.session.get('hashed_password'))
 
		print ""
		print "plain_username is: " + plain_username
		print ""
 
		if (self.check_user_in_database(plain_username) == True):
			print ""
			print str(cherrypy.session.get('username')) + " is already in the database"
			print ""
			return
			#raise cherrypy.HTTPRedirect('/') #You are already in the database
 
		print ""
		print ""
 
		conn = sqlite3.connect('user_database.db')
		c = conn.cursor()

		c.execute("INSERT INTO users (enc_username) VALUES (?)", (plain_username,))

		conn.commit()
		c.close()
		conn.close()

		#raise cherrypy.HTTPRedirect('/')

	@cherrypy.expose
	def change_current_country(self):

		if (cherrypy.session.get('username') == None):
			raise cherrypy.HTTPRedirect('/')

		Page = '<form action="/update_country" method="post" enctype="multipart/form-data">'
		Page += 'Country: <input type="text" name="input_country"/><br/>'
		Page += '<input type="submit" value="Enter"/></form>'

		Page += "Click here to return to your <a href='index'>home page</a>"

		return Page

	@cherrypy.expose
	def update_country(self, input_country=None):

		if (cherrypy.session.get('username') == None):
			raise cherrypy.HTTPRedirect('/')
			#return

		print ""
		print "type(country) returns" + str(type(input_country))
		print ""

		input_country = str(input_country)

		#print "string version of 'country' is: " + input_country
		#print ""

		if not((input_country == None) or (input_country == "")):
			conn = sqlite3.connect('user_database.db')
			c = conn.cursor()

			#c.execute("UPDATE users SET country = " + country + " WHERE username = " + cherrypy.session.get('username'))
			#c.execute("INSERT INTO users (country) VALUES (?) WHERE username = " + cherrypy.session.get('username'), (country,))
			c.execute("UPDATE users SET country = ? WHERE enc_username = ?", (input_country, str(cherrypy.session.get('username'))))
			conn.commit()
			c.close()
			conn.close()
			raise cherrypy.HTTPRedirect('/')
			#return

		raise cherrypy.HTTPRedirect('/')
		#return

	def check_user_in_database(self, username):

		print ""
		print "The 'check_user_in_database' function has been called"
		print ""

		username = str(username)

		conn = sqlite3.connect('user_database.db')
		c = conn.cursor()

		c.execute("SELECT enc_username FROM users")
		username_list = list(c.fetchall())

		#print ""
		#print "Printing list of users"
		#for x in range(0, len(username_list)):
		#	 print username_list[x]

		#print "End of list of users"
		#print ""

		for x in range(0, len(username_list)): #This for loop shortens the username of everyone to just their UPI

			username_list[x] = str(username_list[x])
			username_list[x] = username_list[x][3:]


			character_counter = 0
			while(character_counter < len(username_list[x])):
				if (username_list[x][character_counter] == "'"):
					username_list[x] = username_list[x][0:character_counter]
					character_counter = len(username_list[x])
				else:
					character_counter += 1

		print ""
		print "TESTING - Printing list of users"
		for x in range(0, len(username_list)):
			print username_list[x]

		print "End of list of users"
		print ""
		#print "Printing 'str(cherrypy.session.get('username'))' returns: " + str(cherrypy.session.get('username'))
		#print ""
		#prints "pcas121", without the quotation marks

		if (len(username_list) > 0):
			print "username string is: " + username + " and username_list[0] is " + username_list[0]
			print "Are they equal? " + str(username == username_list[0])
			print ""

		print "Number of entries in user database is: " + str(len(username_list))
		print ""

		for x in range(0, len(username_list)):
			if str(username_list[x]) == username:
				return True

		return False

	@cherrypy.expose
	def change_current_bio(self):

		if (cherrypy.session.get('username') == None):
			raise cherrypy.HTTPRedirect('/')

		Page = '<form action="/update_your_bio" method="post" enctype="multipart/form-data">'
		Page += 'Your Bio: <input type="text" name="personal_bio"/><br/>'
		Page += '<input type="submit" value="Enter"/></form>'

		Page += "Click here to return to your <a href='index'>home page</a>"

		return Page

	@cherrypy.expose
	def update_your_bio(self, personal_bio = None):

		if (cherrypy.session.get('username') == None):
			raise cherrypy.HTTPRedirect('/')

		personal_bio = str(personal_bio)

		if (len(personal_bio) > 600):
			Page = "Your personal biography is too long. Please keep it less than 600 characters."
			Page += "Click here to return to your <a href='index'>home screen</a><br/><br/>"
			return Page

		if not((personal_bio == None) or (personal_bio == "")):

			conn = sqlite3.connect('user_database.db')
			c = conn.cursor()
			c.execute("UPDATE users SET personal_bio = ? WHERE enc_username = ?", (personal_bio, str(cherrypy.session.get('username'))))
			conn.commit()
			c.close()
			conn.close()
			raise cherrypy.HTTPRedirect('/')

		raise cherrypy.HTTPRedirect('/')


	def tidy_up_string(self, untidy_string):

		untidy_string = str(untidy_string)

		if (untidy_string == "[(None,)]"):
			return "NULL"

		#Check to see if the input string contains double quotation marks, such as ". This occurs if a country's name contains an apostraphy

		has_apostraphy = False
		for x in range(0, len(untidy_string)):
			if untidy_string[x] == '"':
				has_apostraphy = True

		if (has_apostraphy):
			#Find the location of both double quotation marks (assumting there are only two), and only keep characters inbetween them
			double_quot_mrk_1 = 0
			while not(untidy_string[double_quot_mrk_1] == '"'):
				double_quot_mrk_1 += 1

			double_quot_mrk_2 = double_quot_mrk_1 + 1
			while not(untidy_string[double_quot_mrk_2] == '"'):
				double_quot_mrk_2 += 1

			if not(double_quot_mrk_2 == (double_quot_mrk_1 + 1)):
				tidy_string = untidy_string[(double_quot_mrk_1 + 1):double_quot_mrk_2]
				return tidy_string
			else:
				return untidy_string

		else:

			#Find location of single quotation marks (assuming there are only two of them), and removing all characters outside of them, including the marks
			quot_mrk_1 = 0
			while not(untidy_string[quot_mrk_1] == "'"):
				quot_mrk_1 += 1

			quot_mrk_2 = quot_mrk_1 + 1
			while not(untidy_string[quot_mrk_2] == "'"):
				quot_mrk_2 += 1

			if not(quot_mrk_2 == (quot_mrk_1 + 1)):
				tidy_string = untidy_string[(quot_mrk_1 + 1):quot_mrk_2]
				return tidy_string
			else:
				return untidy_string


		#counter = 0
		#while not(untidy_string[counter] == "'"):
		#	counter += 1


		#tidy_string = untidy_string[(counter + 1):]

		#counter=0
		#while not(tidy_string[counter] == "'"):
		#	counter += 1

		#tidy_string = tidy_string[:counter]

		#return tidy_string


	@cherrypy.expose
	def display_profile(self):

		print ""
		print "display_profile function has been called"
		print ""

		if (cherrypy.session.get('username') == None):
			print "leaving 'display_profile' function. You don't have a cherrypy session"
			print ""
			raise cherrypy.HTTPRedirect('/')

		Page = str(cherrypy.session.get('username')) + "'s profile:<br/><br/>"

		conn = sqlite3.connect('user_database.db')
		c = conn.cursor()

		#c.execute("SELECT * FROM users WHERE enc_username = ? ", (str(cherrypy.session.get('username')),))
		#profile_list = list(c.fetchall())

		c.execute("SELECT enc_username FROM users WHERE enc_username = ?", (str(cherrypy.session.get('username')), ))
		username = str(c.fetchall())

		c.execute("SELECT country FROM users WHERE enc_username = ?", (str(cherrypy.session.get('username')), ))
		country = str(c.fetchall())

		c.execute("SELECT personal_bio FROM users WHERE enc_username = ?", (str(cherrypy.session.get('username')), ))
		personal_bio = str(c.fetchall())


		#Make a separate function to tidy-up the new strings

		print ""
		print "about to tidy_up the username"
		username = self.tidy_up_string(username)


		#has_apostraphy = False
		#for x in range(0, len(country)):
		#	if (country[x] == "'"):
		#		has_apostraphy = True

		#if (has_apostraphy):
		#	print ""
		#	print "about to tidy_up the country"
		#	country = self.tidy_up_string(country)

		country = self.tidy_up_string(country)

		personal_bio = self.tidy_up_string(personal_bio)

		#has_apostraphy = False
		#for x in range(0, len(personal_bio)):
		#	if (personal_bio[x] == "'"):
		#		has_apostraphy = True

		#if (has_apostraphy):
		#	print ""
		#	print "about to tidy_up the personal_bio"
		#	personal_bio = self.tidy_up_string(personal_bio)

		Page += "Username: " + username + "<br/>"
		Page += "Country: " + country + "<br/>"
		Page += "Biography: " + personal_bio + "<br/>"

		print "Username: " + username
		print "Country: " + country
		print "Biography: " + personal_bio

		#print "profile_list size is: " + str(len(profile_list))

		#print ""
		#for x in range(0, len(profile_list)):
		#	if (x==0):
		#		username = str(profile_list[x])

			#print str(profile_list[x])
			#Page += str(profile_list[x]) + "<br/>"
		#print ""

		Page += "<br/>Click here to return to your <a href='index'>home page</a>"

		return Page

	#@cherrypy.expose
	#def display_persons_profile(self, ):


	def create_messages_table(self):
		"""This will create a table for messages if, and only if, there isn't already a table"""

		conn = sqlite3.connect('messages.db') #connection
		c = conn.cursor() #cursor

		#usernames should be encrypted, as if username and hashed_password are both known, a person can still login to the person's account via browser

		c.execute("CREATE TABLE IF NOT EXISTS messages(sender TEXT, recepient TEXT, message TEXT)")
		c.close()
		conn.close()




def runMainApp():
	cherrypy.tree.mount(MainApp(), "/")

	cherrypy.config.update({'server.socket_host': listen_ip,
							'server.socket_port': listen_port,
							'engine.autoreload.on': True,
							})

	print "========================================"
	print "               WELCOME"
	print "           this is pcas121"
	print "     COMPSYS302 Assignment 2 - 2018"
	print "========================================"

	cherrypy.engine.start()

	cherrypy.engine.block()


runMainApp()
